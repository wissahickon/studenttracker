<?php
include(__DIR__.'/../../db/student.php');
if(!isset($_GET['studentid']))
	die('studentid not specified');
$studentid = mysqli_real_escape_string($con, $_GET['studentid']);
echo json_encode(getStudent($studentid));
?>