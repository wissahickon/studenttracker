<?php
include(__DIR__.'/../../db/session.php');
if(isset($_GET['image'])) {
	$image = mysqli_real_escape_string($con, $_GET['image']);
	$filename = "../../../Pictures/{$image}.jpg";
	$file_handle = fopen($filename, "r");
	$contents = fread($file_handle, filesize($filename));
	
	header('Content-Type: image/jpeg');
	echo $contents;
	
	fclose($file_handle);
}
?>