<?php
include(__DIR__.'/../../db/users.php');

if(!isset($_POST['user']))
	die('user not specified');
if(!isset($_POST['pass']))
	die('pass not specified');
if(!isset($_POST['perm']))
	die('perm not specified');

$user = mysqli_real_escape_string($con, $_POST['user']);
$pass = mysqli_real_escape_string($con, $_POST['pass']);
$perm = mysqli_real_escape_string($con, $_POST['perm']);

createUser($user, $pass, $perm);
?>