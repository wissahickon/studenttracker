<?php
include(__DIR__.'/../../db/rooms.php');

if(!isset($_GET['room']))
	die('room not specified');

$room = mysqli_real_escape_string($con, $_GET['room']);

echo json_encode(getRoomUsers($room));
?>